import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setProducts } from './redux/actions/productActions';
import axios from 'axios';
import Productlist from '../components/Productlist';

const Product = () => {
    const product =useSelector((state)=>state);
    const dispatch =useDispatch();

    const fetchproducts =async()=>{
        const response =await axios
        .get("https://backend.sports.info/api/v1/nba/game/f34b1dfd-97fd-4942-9c14-05a05eeb5921/").catch((err)=>{
            console.log('err',err);
        });
        dispatch(setProducts(response.data));
    };
    useEffect(()=>{
        fetchproducts();
    },[])
    console.log(product)
  return (
    <div>
        <Productlist/>
    </div>
  )
}

export default Product