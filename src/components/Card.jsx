import React from 'react'
import '../css/card.css'

const Card = ({data}) => {
console.log(data)
const {away, home,}=data
const date = new Date(data.scheduled)

  return (
    <div className='maincard'>
    <div className='subcard'>
    <div className='hadding'>
    <p>Wells Fargom Center,{home.market}{date.toDateString()}</p>
    </div>
    
   <div className='center'>
    <div className='philadelphia'>
    <p className='home-p'>
     {home.market}

     {home.name}
  <br></br>
   <span className='home-s'>
   {home.points}
   </span>
    </p>
    </div>

    
<div className='sub table'>
<table className='table'>
  <tr>
  <td></td>
    <td> {away.scoring[0].number}</td>
    <td>{away.scoring[1].number}</td>
    <td>{away.scoring[2].number}</td>
    <td>{away.scoring[3].number}</td>
    <td>T</td>
   </tr>
   <tr>
   <td className='color'>{home.alias}</td>
    <td className='color'> {home.scoring[0].points}</td>
    <td>{home.scoring[1].points}</td>
    <td>{home.scoring[2].points}</td>
    <td className='color'>{home.scoring[3].points}</td>
    <td> {home.points}</td>
   </tr>
   <tr>
   <td className='color'>{away.alias}</td>
    <td> {away.scoring[0].points}</td>
    <td className='color'>{away.scoring[1].points}</td>
    <td className='color'>{away.scoring[2].points}</td>
    <td>{away.scoring[3].points}</td>
    <td className='color'> {away.points}</td>
   </tr>
   </table>
   </div>
   <div className='Toronto'>
   <p className='away-p'>
    {away.market}
    {away.name}
    <br/>
      <span className='away-s'>
      {away.points}
      </span>
    </p>
   </div>

   </div>
    </div>


    </div>
  )
}

export default Card