import { createStoreHook } from "react-redux";
import reducers from "./containers/redux/reducers";

const store =createStoreHook(reducers,{});

export default store